import React from "react";
import Menu from "./Menu";
import "../styles.css";
import { Carousel } from 'react-bootstrap';
import {Link} from 'react-router-dom'
import logo from '../logo.jpg'

const Layout = ({title = "Title", description = "Description", className, children}) => (

<div>
	<Menu />

	{/*<div className="jumbotron">
		<h2 class="inline-block  ">{title}</h2>
		<p className="">{description}</p>
	


	</div>*/}


	<div className={className} >{children}</div>
	

	<footer class="footer-distributed">

			<div class="footer-left">
          <img src={logo} />
				<h3>About <span>Gelato Express</span></h3>

				<p class="footer-links">
					<a href="#" class="mx-2">Home</a>
					|
					<a href="#" class="mx-2">Blog</a>
					|
					<a href="#" class="mx-2">About</a>
					|
					<a href="#" class="mx-2">Contact</a>
				</p>

				<p class="footer-company-name">© 2021 Gelato Express</p>
			</div>

			<div class="footer-center">
				<div>
					<i class="fa fa-map-marker"></i>
					  <p><span>29 Venus Street, Brgy. Tandang Sora, Quezon City, 
					  </span>
						Metro Manila, Philippines - 1116</p>
				</div>

				<div>
					<i class="fa fa-phone"></i>
					<p>+639171893696</p>
				</div>
				<div>
					<i class="fa fa-envelope"></i>
					<p><a href="mailto:gelatoexpress@gmail.com">gelatoexpress@gmail.com</a></p>
				</div>
			</div>
			<div class="footer-right">
				<p class="footer-company-about">
					<span>About the company</span>
					It’s all about artisanal, premium scoops at gelatoexpress.ph</p>
				<div class="footer-icons">
					<a href="#"><i class="fa fa-facebook"></i></a>
					<a href="#"><i class="fa fa-twitter"></i></a>
					<a href="https://www.instagram.com/gelatoexpress.ph/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a>
					<a href="#"><i class="fa fa-linkedin"></i></a>
					<a href="#"><i class="fa fa-youtube"></i></a>
				</div>
			</div>
		</footer>


</div>
)
export default Layout;