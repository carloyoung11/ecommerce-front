import React, {Fragment} from 'react'
import {Link, withRouter, NavLink} from 'react-router-dom'
import { Navbar, Nav } from 'react-bootstrap';
import {signout, isAuthenticated} from '../auth'
import {itemTotal} from './cartHelpers'; 
import logo from '../logo.jpg'
import Search from './Search'

const isActive = (history, path) => {
	if(history.location.pathname === path) {
		return {color: '#000000'}
	} else {
		return {color: '#000000'}
	}
}

const Menu = ({history}) => (
	
	
	<Navbar bg="" expand="lg">
	<Navbar.Brand as={Link} to="/" exact><img src={logo} width="190" height="120" id="logo" /></Navbar.Brand>
		<Navbar.Toggle aria-controls="basic-navbar-nav" />
	<Navbar.Collapse id="basic-navbar-nav">
	<Nav className="ml-auto">
		<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/')} to="/" exact>Home</Nav.Link>
		<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/shop')} to="/shop" exact>Shop</Nav.Link>
		<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/cart')} to="/cart"><i class="fa fa-shopping-cart" aria-hidden="true"></i>
 		<sup><small className="cart-badge">{itemTotal()}</small></sup></Nav.Link>
 		{isAuthenticated() && isAuthenticated().user.role === 0 && (
			<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/user/dashboard')} to="/user/dashboard"> <i class="fa fa-user" aria-hidden="true"></i> </Nav.Link>
			)}
		{isAuthenticated() && isAuthenticated().user.role === 1 && (
			<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/admin/dashboard')} to="/admin/dashboard"> 
		<i class="fa fa-user" aria-hidden="true"></i> </Nav.Link>
			)}
		{!isAuthenticated() && (
			<Fragment>
			<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/signin')} to="/signin">Signin</Nav.Link>
		
			<Nav.Link as={NavLink} className="nav-link" style={isActive(history,'/signup')} to="/signup">Signup</Nav.Link>
			</Fragment>
			)}
			{isAuthenticated() && (
			<span 
				className="nav-link"
			 	style={{cursor: 'pointer', color: '#000000'}} 
			 	onClick={() => signout(() => {
			 		history.push('/')
			 	})
			 }							 	
			 >
			 	<i class="fa fa-sign-out" aria-hidden="true"></i>
			 	</span>


			)}
			</Nav>
          </Navbar.Collapse>
	 </Navbar>



	)

export default withRouter(Menu)