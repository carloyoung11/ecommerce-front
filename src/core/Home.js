import React, {useState, useEffect} from "react";
import Layout from './Layout'
import{getProducts} from './apiCore'
import Card from './Card' 
import Search from './Search'
import { Carousel } from 'react-bootstrap';
import {Link} from 'react-router-dom'

import img1 from '../g-1.jpg'
import img2 from '../g-2.jpg'
import img3 from '../g-3.jpg'

const Home = () => {
	const [productsBySell, setproductsBySell] = useState([])
	const [productsByArrival, setproductsByArrival] = useState([])
	const [error, setError] = useState(false)

	
	const loadProductsBySell = () => {
		getProducts('sold').then(data => {
			if(data.error) {
				setError(data.error)
			} else {
				setproductsBySell(data)
			}
		})
	}

	const loadProductsByArrival = () => {
		getProducts('createdAt').then(data => {
			if(data.error) {
				setError(data.error)
			} else {
				setproductsByArrival(data)
			}
		})
	}

	useEffect(() => {
		loadProductsByArrival()
		loadProductsBySell()
	}, [])

	return (
		
		<Layout>
		
		<div className="landing container-fluid">
					<div class="row">
			<div class="col-md-6 order-2 order-md-2 d-flex flex-column justify-content-center align-items-center">
			<h1 className="text-center mx-auto">
				We Serve Artisanal Gelato

			</h1>
			<p class="text-center text-dark mx-auto"> It’s all about artisanal, premium scoops at gelatoexpress.ph. There’s an ice-cream flavor for everyone here: Try the Nocciola (Hazelnut) (P380) if you like it nutty, the Belgian Chocolate (P360) if your idea of a good chocolate dessert is one that’s full of chunks of cookie dough, or the Caffe (P350) if you find yourself constantly yearning for some caffeine. 
			</p>
			<Link to="/shop">
			<button className="btn btn-light mr-2 my-2 w-45" id="orderNow">Order Now!</button>
			</Link>
			</div>
			
			<div class="col-md-6 order-1 order-md-1 d-flex flex-column justify-content-center" id="carousel1">
			<Carousel fade class="pb-4"> 
 			 <Carousel.Item>
    			<img
      			className="w-100 img-fluid"
      			src={img1}
      			alt="First slide"
      			id="img5"
    			/>
    <Carousel.Caption>
      {/*<h3 class="text-dark">Chocolate</h3>*/}
     {/* <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>*/}
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="w-100 img-fluid"
      src={img2}
      alt="Second slide"
      id="img5"
    />

    <Carousel.Caption>
      {/*<h3 class="text-dark">Apple Crumble</h3>*/}
      {/*<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>*/}
    </Carousel.Caption>
  </Carousel.Item>
  <Carousel.Item>
    <img
      className="w-100 img-fluid"
      src={img3}
      alt="Third slide"
      id="img5"
    />

    <Carousel.Caption>
      {/*<h3 class="text-dark">Caffe</h3>*/}
     {/* <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>*/}
    </Carousel.Caption>
  </Carousel.Item>
</Carousel>	
	</div>
		</div>
</div>
		
		<h2 className="mb-4 ml-3 mt-3 pt-5 text-dark">Best Sellers</h2>
		<div className ="d-flex flex-row justify-content-center flex-wrap p-5 m-5 mx-auto">
			
			{productsBySell.map((product, i) => (
			<div key={i} className="m-3 p-3 col-lg-3 col-md-6 col-sm-12">
			<Card  product={product} />
			</div>
			))}

		</div>
		
		<h2 className="mb-4 ml-3 text-dark">New Arrivals</h2>
		<div className ="d-flex flex-row justify-content-center flex-wrap p-5 m-5 mx-auto">
		{productsByArrival.map((product, i) => (
			<div key={i} className="m-3 p-3 col-lg-3 col-md-6 col-sm-12">
			<Card  product={product} />
			</div>
			))}
		</div>
		
		</Layout>

		)
}

export default Home;