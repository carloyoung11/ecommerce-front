import {API} from "../config"

export const createCategory = (userId, token, category) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/category/create/${userId}`, {
			method: "POST",
			headers: {
				Accept: 'application/json',
				"Content-Type": "application/json",
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify(category)
		})
		.then(response => {
			return response.json()
		})
		.catch(err => {
			console.log(err)
		})

	}

export const createProduct = (userId, token, product) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/product/create/${userId}`, {
			method: "POST",
			headers: {
				Accept: 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: product
		})
		.then(response => {
			return response.json()
		})
		.catch(err => {
			console.log(err)
		})

	}


export const getCategories = () => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/categories`, {
			method: "GET"
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}


export const listOrders = (userId,token) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/order/list/${userId}`, {
			method: "GET",
			headers: {
				Accept: 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}


export const getStatusValues = (userId,token) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/order/status-values/${userId}`, {
			method: "GET",
			headers: {
				Accept: 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}


export const updateOrderStatus = (userId,token,orderId,status) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/order/${orderId}/status/${userId}`, {
			method: "PUT",
			headers: {
				Accept: 'application/json',
				"Content-Type": 'application/json',
				Authorization: `Bearer ${token}`
			},
			body: JSON.stringify({status, orderId})
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}


// PERFORM CRUD
// get all products
// get a product
// update single product
// delete single product


export const getProducts = () => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/products?limit=100`, {
			method: "GET"
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}

export const deleteProduct = (productId, userId, token) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/product/${productId}/${userId}`, {
			method: "DELETE",
			headers: {
				Accept: 'application/json',
				"Content-Type": 'application/json',
				Authorization: `Bearer ${token}`
			}
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}

export const getProduct = (productId) => {
		return fetch(`https://whispering-headland-81510.herokuapp.com/api/product/${productId}`, {
			method: "GET"
		})
		.then(response => {
			return response.json();
		})
		.catch(err => console.log(err))
	}


export const updateProduct = (productId, userId, token, product) => {
    return fetch(`https://whispering-headland-81510.herokuapp.com/api/product/${productId}/${userId}`, {
        method: 'PUT',
        headers: {
            Accept: 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: product
    })
        .then(response => {
            return response.json();
        })
        .catch(err => console.log(err));
};
























