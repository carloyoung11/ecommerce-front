import React, {useState, useEffect} from 'react'
import Layout from '../core/Layout'
import { isAuthenticated } from '../auth'
import {Link, Redirect} from 'react-router-dom'
import {read, update, updateUser} from'./apiUser'

const Profile = ({match}) => {
	const [values, setValues] = useState({
		name: '',
		email: '',
		password: '',
		error: false,
		success: false
	})

	const {token} =isAuthenticated()


	const {name, email, password, error, success} = values

	const init = (userId) => {
		// 
		read(userId, token).then(data => {
			if(data.error) {
				setValues({...values, error: true})
			} else {
				setValues({...values, name:data.name, email: data.email})
			}
		})

	}

	useEffect(() => {
		init(match.params.userId)
	}, [])

	const handleChange = name => e => {
		setValues({...values, error: false, [name]: e.target.value})
	}

	const clickSubmit = (e) => {
		e.preventDefault()
		update(match.params.userId, token, {name, email, password}).then(data => {
			if(data.error) {
				console.log(data.error)
			} else {
				updateUser(data, () => {
					setValues({...values, name:data.name, email: data.email, success: true
					})
				})
			}
		}) 

	}

	const redirectUser = (success) => {
		if(success) {
			return <Redirect to="/cart" />
		}
	}

	const profileUpdate = (name, email, password) => (
		<form class="col-10 col-md-8 mx-auto d-flex flex-column">
			<div className="form-group">
			<label className="text-dark">Name</label>
			<input 
				type="text" 
				onChange={handleChange('name')}
				className="form-control"
				value={name}/>
			</div>
			<div className="form-group">
			<label className="text-dark">Email</label>
			<input 
				type="email" 
				onChange={handleChange('email')}
				className="form-control"
				value={email}/>
			</div>
			<div className="form-group">
			<label className="text-dark">Password</label>
			<input 
				type="password" 
				onChange={handleChange('password')}
				className="form-control"
				value={password}/>
			</div>

			<button onClick={clickSubmit} className="btn btn-primary">Submit</button>


		</form>
	)

	return (
		<Layout title="Profile" description="Update your Profile" className="container-fluid">
		<h1 className="mb-4 text-white text-center">Profile Update</h1>
		{profileUpdate(name, email, password)}
		{redirectUser(success)}

		</Layout>

		)
}



export default Profile;